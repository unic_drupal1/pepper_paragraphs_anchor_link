<?php

namespace Drupal\pepper_paragraphs_anchor_link\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'anchor link' widget.
 *
 * @FieldWidget(
 *   id = "anchor_link",
 *   label = @Translation("Anchor Link"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class AnchorLinkWidget extends StringTextfieldWidget {

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // validate if input is a valid achnor link.
    $element['#element_validate'][] = [$this, 'validateAnchorField'];
    return parent::formElement($items, $delta, $element, $form, $form_state);
  }

  /**
   * Validates whether the input is a valid anchor link.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The form array.
   */
  public static function validateAnchorField(array $element, FormStateInterface $form_state, array $form) {
    if (isset($element['#value']) && !empty($element['#value'])) {
      $title = !empty($element['#title']) ? $element['#title'] : '';
      $anchor_field = $element['#value'];

      // Check for special characters
      if (!empty($anchor_field) && !preg_match('/^[a-z 0-9 -]+$/', $anchor_field)) {
        $form_state->setError($element, t('Das @name  Feld darf keine Leerzeichen, Grossbuchstaben oder Sonderzeichen enthalten.', ['@name ' => $title]));
      }

    }
  }

}
